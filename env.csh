set machine=`gcc -dumpmachine`
set version=`gcc -dumpversion`

setenv WR_SYS /mbs/driv/white_rabbit/fallout/${GSI_CPU_PLATFORM}_${GSI_OS}_${GSI_OS_VERSIONX}_${GSI_OS_TYPE}

setenv NURDLIB_DEF_PATH $EXP_PATH/nurdlib/cfg/default
if ("x$is_rio" != "x") then
	set trloiipath=$EXP_PATH/trloii
	set trloctrl=`ls $trloiipath/trloctrl`
	setenv TRLOII_PATH  $trloiipath
	setenv RFX0_FW      `echo "$trloctrl" | tr ' ' '\n' | grep _rfx0$ | head -n1 | sed -n 's/fw_\(.*\)_rfx0/\1/p'`
	setenv RFX1_FW      `echo "$trloctrl" | tr ' ' '\n' | grep _rfx1$ | head -n1 | sed -n 's/fw_\(.*\)_rfx1/\1/p'`
	setenv TRIDI_FW     `echo "$trloctrl" | tr ' ' '\n' | grep _tridi$ | head -n1 | sed -n 's/fw_\(.*\)_tridi/\1/p'`
	setenv VULOM4_FW    `echo "$trloctrl" | tr ' ' '\n' | grep _trlo$ | head -n1 | sed -n 's/fw_\(.*\)_trlo/\1/p'`
	echo RFX0=$RFX0_FW RFX1=$RFX1_FW TRIDI=$TRIDI_FW VULOM4=$VULOM4_FW
	setenv RFX0_CTRL    $trloiipath/trloctrl/fw_${RFX0_FW}_tridi/bin_${machine}_${version}/rfx0_ctrl
	setenv RFX1_CTRL    $trloiipath/trloctrl/fw_${RFX1_FW}_tridi/bin_${machine}_${version}/rfx1_ctrl
	setenv TRIDI_CTRL   $trloiipath/trloctrl/fw_${TRIDI_FW}_tridi/bin_${machine}_${version}/tridi_ctrl
	setenv TRIMI_CTRL   $trloiipath/trimictrl/bin_${machine}_${version}/trimictrl
	setenv TRLOII_FLASH $trloiipath/flash/bin_${machine}_${version}/vulomflash
	setenv VULOM4_CTRL  $trloiipath/trloctrl/fw_${VULOM4_FW}_trlo/bin_${machine}_${version}/trlo_ctrl
endif
