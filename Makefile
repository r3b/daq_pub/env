pwd:=$(shell pwd)
src=$(pwd)/env.csh
run=$(pwd)/Makefile
dst=$(pwd)/env.sh

$(dst): $(src) $(run)
	@echo "$@"
	@(echo "# This file ($(dst)) was generated from $(src) by $(run), please edit the latter two and re-login!" && sed 's/alias \([_A-Za-z0-9]*\) \(.*\)/alias \1="\2"/;s/set //;s/setenv \([_A-Za-z0-9]*\) /export \1=/;s/if (/if [ /;s/) then/ ]; then/;s/endif/fi/' $(src)) > $(dst)
